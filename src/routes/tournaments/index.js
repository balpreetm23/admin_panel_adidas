import React,{Component} from 'react';
import PropTypes from 'prop-types'
import { routerRedux } from 'dva/router'
import { connect } from 'dva'
import { Button, Modal, DatePicker, Select, Switch } from 'antd'
import { Page } from 'components'
import queryString from 'query-string'
// import Modal from './Modal'
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import Table from '../table'
import request from 'superagent';
const Option = Select.Option;
import config from '../../utils/config'
const columns = [
   {
    Header: 'Name',
    accessor: 'name',
    key: 'name',
    filterMethod: (filter, row) =>
              row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
    Cell: props=> (
      <div>
        <a href={'/tournaments/'+props.original.id}>{props.value}</a>
      </div>
    )
  }, {
    Header: 'Start Date',
    accessor: 'start_date',
    filterable:false,
    key: 'start_date',
    Cell: props=> (
      <div>
        {new Date(Date.parse(props.value)).toDateString()}
      </div>
    )

  }, {
    Header: 'No of Teams',
    accessor: 'team',
    filterable:false,
    Cell: props=> (
      <div>
        {props.value.length}
      </div>
    ),
    key: 'team',
  }, {
    Header: 'City/State',
    accessor: 'city',
    key: 'city',
    filterMethod: (filter, row) =>
              row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
  },
]


class Tournaments extends Component {
  constructor(props){
    super(props);

    this.state = { modalVisible:false, tournamentData:[] };
    console.log('config',config)
  }

  componentDidMount(){
    request
    .post(config.apiURL+'/tournament/v1/listTournament/')
    .set('Content-Type', 'application/json')
    .send({
      latitude:28.6998095,
      longitude:77.2082712,
      max_distance:"200",
      max_entry:"50000",
      max_prize:5000000,
      min_distance:"0",
      min_entry:"0",
      min_prize:"0",
      page:1,
      type_of_team:["corporate", "open-to-all", "mix-corporate", "school", "college", "academy"],
      type_of_tournament:[ "league"]
    })
    .then(result => {
      let res = JSON.parse(result.text)
      if (res.success) {
        console.log(res)
        this.setState({ tournamentData:res.data })
      }
    })
  }

  handleAccount(event) {
    this.setState({ modalVisible:false })
    event.preventDefault()
  }

  render(){
    console.log('Render', this.state)
    return (
    <div>
      <div className="d-none">
        <Button icon="plus-square" size={'large'} onClick = {() => this.setState({ modalVisible:true })}>Create Tournament</Button>
      </div>
      <Modal
        title="Add a Tournament"
        wrapClassName="vertical-center-modal"
        visible={this.state.modalVisible}
        onOk={() => this.handleAccount.bind(this)}
        onCancel={() => this.setState({ modalVisible:false })}
      >
      <form>
        <div className="form-group row">
          <label htmlFor="inputName" className="col-sm-4 col-form-label"><h6>Name</h6></label>
          <div className="col-sm-8">
            <input type="text" className="form-control" id="inputName" placeholder="Name" />
          </div>
        </div>
        <div className="form-group row">
          <label className="col-sm-4 col-form-label"><h6>Sport</h6></label>
          <div className="col-sm-8">
            <Select
              showSearch
              style={{ width: '100%' }}
              placeholder="Select Sport"
              optionFilterProp="children"
              onChange={(val) => console.log(val)}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              <Option value="jack">Jack</Option>
              <Option value="lucy">Lucy</Option>
              <Option value="tom">Tom</Option>
            </Select>

          </div>
        </div>
        <div className="form-group row">
          <label className="col-sm-4 col-form-label"><h6>Competitor Type</h6></label>
          <div className="col-sm-8">
            <Select
              showSearch
              style={{ width: '100%' }}
              placeholder="Select Competitor Type"
              optionFilterProp="children"
              onChange={(val) => console.log(val)}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              <Option value="jack">Jack</Option>
              <Option value="lucy">Lucy</Option>
              <Option value="tom">Tom</Option>
            </Select>

          </div>
        </div>
        <div className="form-group row">
          <label className="col-sm-4 col-form-label"><h6>Tournament Type</h6></label>
          <div className="col-sm-8">
            <Select
              showSearch
              placeholder="Select Tournament Type"
              style={{ width: '100%' }}
              optionFilterProp="children"
              onChange={(val) => console.log(val)}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              <Option value="jack">Jack</Option>
              <Option value="lucy">Lucy</Option>
              <Option value="tom">Tom</Option>
            </Select>
          </div>
        </div>
        <div className="form-group row">
          <label className="col-sm-5 col-form-label"><h6>Register Start Date</h6></label>
          <div className="col-sm-7">
            <DatePicker size={'large'} placeholder="Register Start Date" onChange={(val) => console.log(val)} />
          </div>
        </div>

        <div className="form-group row">
          <label className="col-sm-5 col-form-label"><h6>Register End Date</h6></label>
          <div className="col-sm-7">
            <DatePicker size={'large'} placeholder="Register End Date" onChange={(val) => console.log(val)} />
          </div>
        </div>

        <div className="form-group row">
          <label className="col-sm-5 col-form-label"><h6>Tournament Start</h6></label>
          <div className="col-sm-7">
            <DatePicker size={'large'} placeholder="Tournament Start" onChange={(val) => console.log(val)} />
          </div>
        </div>

        <div className="form-group row">
          <label className="col-sm-5 col-form-label"><h6>Tournament End</h6></label>
          <div className="col-sm-7">
            <DatePicker size={'large'} placeholder="Tournament End" onChange={(val) => console.log(val)} />
          </div>
        </div>

        <div className="form-group">
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <span className="input-group-text">Logo Upload</span>
            </div>
            <div className="custom-file">
              <input type="file" className="custom-file-input" id="logoUpload" />
              <label className="custom-file-label" htmlFor="logoUpload">Choose file</label>
            </div>
          </div>
        </div>

        <div className="form-group">
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <span className="input-group-text">Cover Upload</span>
            </div>
            <div className="custom-file">
              <input type="file" className="custom-file-input" id="coverUpload" />
              <label className="custom-file-label" htmlFor="coverUpload">Choose file</label>
            </div>
          </div>
        </div>

        <div className="form-group row">
          <label className="col-sm-4 col-form-label"><h6>Venues</h6></label>
          <div className="col-sm-8">
            <Select
              showSearch
              placeholder="Select Venue"
              style={{ width: '100%' }}
              optionFilterProp="children"
              onChange={(val) => console.log(val)}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              <Option value="jack">Jack</Option>
              <Option value="lucy">Lucy</Option>
              <Option value="tom">Tom</Option>
            </Select>
          </div>
        </div>

        <div className="form-group row">
          <label className="col-sm-4 col-form-label"><h6>City</h6></label>
          <div className="col-sm-8">
            <Select
              showSearch
              placeholder="Select City"
              style={{ width: '100%' }}
              optionFilterProp="children"
              onChange={(val) => console.log(val)}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              <Option value="jack">Jack</Option>
              <Option value="lucy">Lucy</Option>
              <Option value="tom">Tom</Option>
            </Select>
          </div>
        </div>

        <div className="form-group row">
          <label className="col-sm-4 col-form-label"><h6>No of Teams</h6></label>
          <div className="col-sm-8">
            <Select
              showSearch
              placeholder="Number of Teams"
              style={{ width: '100%' }}
              optionFilterProp="children"
              onChange={(val) => console.log(val)}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              <Option value="jack">Jack</Option>
              <Option value="lucy">Lucy</Option>
              <Option value="tom">Tom</Option>
            </Select>
          </div>
        </div>

        <div className="form-group row">
          <label htmlFor="inputWinningReward" className="col-sm-4 col-form-label"><h6>Winning Reward</h6></label>
          <div className="col-sm-8">
            <input type="text" className="form-control" id="inputWinningReward" placeholder="Winning Reward" />
          </div>
        </div>

        <div className="form-group row">
          <label htmlFor="inputRegistrationFees" className="col-sm-4 col-form-label"><h6>Registration Fee</h6></label>
          <div className="col-sm-8">
            <input type="text" className="form-control" id="inputRegistrationFees" placeholder="Registration Fee" />
          </div>
        </div>

        <div className="form-group row">
          <label htmlFor="showOnApp" className="col-sm-4 col-form-label"><h6>Show on app</h6></label>
          <div className="col-sm-8">
            <Switch defaultChecked onChange={(val) => {console.log(val)}} />,
          </div>
        </div>



      </form>
      </Modal>

      {this.state.tournamentData.length > 0 &&
        <Table columns = {columns} data = {this.state.tournamentData} />
      }
    </div>
    );
  }
}

Tournaments.propTypes = {
  user: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
  loading: PropTypes.object,
}

export default connect(({ user, loading }) => ({ user, loading }))(Tournaments)
