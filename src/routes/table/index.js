import React,{Component} from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';


class Table extends Component {
  constructor(props){
    super(props);

    this.state = {columns:this.props.columns, data:this.props.data};
  }
  render(){
    return (
    <div>
      <ReactTable
        data={this.state.data}
        columns={this.state.columns}
        filterable={true}
        defaultPageSize={10}

        className="-striped -highlight"
      />

    </div>
    );
  }
}

export default Table;
