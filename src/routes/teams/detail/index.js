import React,{Component} from 'react';
import { connect } from 'dva'
import { Modal, Select, DatePicker, Button, Popconfirm } from 'antd'
import Table from '../../table'

import request from 'superagent'
import config from '../../../utils/config'

const Option = Select.Option;



class Detail extends Component {
  constructor(props){
    super(props);
    let ownerColumns = [
      {
        Header: 'Owner Name',
        accessor: 'name',
        key: 'name',
        filterMethod: (filter, row) =>
        row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
      }, {
        Header: 'Owner Image',
        accessor: 'image',
        filterable:false,
        key: 'image',
        Cell: props=> <a href={config.apiURL.replace('/api', '/') + props.value} target="_blank">{config.apiURL.replace('/api', '/') + props.value}</a>

      }, {

        Header: 'Action',
        accessor: 'id',
        key: 'id',
        filterable:false,
        Cell: props=> (

            <Popconfirm
                title="Are you sure delete this Team Owner?"
                onConfirm= {() => this.removeOwner(props.value)}
                okText="Yes"
                cancelText="No"
              >
                <Button type="danger" shape="circle" icon="close" />
              </Popconfirm>
        ),
        maxWidth:75
      },
    ]

    let playerColumns = [
      {
        Header: 'Player Name',
        accessor: 'name',
        key: 'name',
        filterMethod: (filter, row) =>
        row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
      }, {
        Header: 'Phone Number',
        accessor: 'createTime',
        key: 'createTime',
      }, {
        Header: 'Date Added',
        accessor: 'createTime',
        filterable:false,
        key: 'createTime',
      }, {
        Header: 'Action',
        accessor: 'address',
        key: 'address',
        filterMethod: (filter, row) =>
        row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
      },
    ]

    let teamId = this.props.match.params.id
    this.state = {
      teamId,
      teamData:{},
      ownerData:[],
      ownerModal:false,
      playerModal:false,
      ownerColumns,
      playerColumns,
      loading:false,
      team_name:''
    };

  }

  componentDidMount(){
    // let teamId = this.props.match.params.id
    // this.setState({teamId}, ()=>{
      this.getData()
      this.getOwners()
      this.getPlayers()
    // })
  }

  addOwner(){
    this.setState({ loading:true }, () => {
      let teamId = this.state.teamId
      let name = this.refs.ownerName.value
      let image = this.refs.ownerImage.files[0]
      console.log(teamId, name, image)

      request
      .post(config.apiURL+'/tournament/v1/team_owner/')
      .field('team_id',teamId)
      .field('name', name)
      .attach('image', image)
      .then((result) => {
        let res = JSON.parse(result.text)
        if (res.status) {
          console.log(res)
          this.getOwners()
          this.setState({ ownerModal: false, loading:false })
          // this.setState({ tournamentData:res })
        }

      })

    })


  }

  removeOwner(id){
    console.log(id)
    request
    .delete(config.apiURL+'/tournament/v1/team_owner/?id='+id)
    .then((result) => {
      let res = JSON.parse(result.text)
      if (res.status) {
        this.getOwners()
      }
    })
  }

  addPlayer(){

  }

  getOwners(){
    // tournament/v1/gameweek/
    request
    .get(config.apiURL+'/tournament/v1/team_owner?id='+this.state.teamId)
    .then(result => {
      let res = JSON.parse(result.text)
      if (res.status) {
        console.log(res)
        this.setState({ ownerData:res.data, team_name:res.team_name })
      }
    })
  }

  getData(){
    request
    .get(config.apiURL+'/venue/team_player/?id='+this.state.team_id)
    .then(result => {
      let res = JSON.parse(result.text)
      if (res.success) {
        console.log(res)
        this.setState({ teamData:res.data })
      }
    })
  }

  getPlayers(){

  }

  uniqueKey(){
    return new Date().getTime() + Math.random()
  }


  render(){
    console.log('State', this.state)
    return (
    <div key = {this.uniqueKey()}>
      {this.state.team_name.length > 0 &&
        <div className="row hdr mb-3 ">
          <div className="col-md-6">
            <h3>{this.state.team_name}</h3>
          </div>
          <div className="col-md-6 d-none">
            <button className="btn btn-outline-dark col-md-6 offset-md-6">Edit Team</button>
          </div>
        </div>
      }

      <div className="row">
        <div className="col-md-6">
          <h6>List Of Owners </h6>
        </div>
        <div className="col-md-6">
          <button className="btn btn-outline-dark col-md-6 offset-md-6" onClick = {() => {this.setState({ ownerModal:true })}} >Add Owner</button>
        </div>
      </div>
      <Table key = {this.uniqueKey()} columns = {this.state.ownerColumns} data = {this.state.ownerData} />

      <hr/>
      <div className="row d-none">
        <div className="col-md-6">
          <h6>List of Players </h6>
        </div>
        <div className="col-md-6">
          <button className="btn btn-outline-dark col-md-6 offset-md-6" onClick = {() => {this.setState({ playerModal:true })}}>Add Player</button>
        </div>
        <Table columns = {this.state.playerColumns} data = {this.state.playerData} />
      </div>
      <hr/>
      {/* Modals are as follows */}
      <Modal
        title="Add an Owner"
        wrapClassName="vertical-center-modal"
        visible={this.state.ownerModal}
        onOk={this.addOwner.bind(this)}
        onCancel={() => this.setState({ ownerModal:false })}
        confirmLoading={this.state.loading}
      >
        <form>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label"><h6>Owner Name</h6></label>
            <div className="col-sm-8">
              <input type="text" ref="ownerName" className="form-control" placeholder="Add Team Owner Name here" />
            </div>
          </div>

          <div className="form-group">
            <input type="file" className="form-control" ref="ownerImage" id="logoUpload" />
          </div>

        </form>
      </Modal>

      <Modal
        title="Add a Player"
        wrapClassName="vertical-center-modal"
        visible={this.state.playerModal}
        onOk={() => this.addPlayer.bind(this)}
        onCancel={() => this.setState({ playerModal:false })}
      >
        <form>
          <div className="form-row">
            <div className="form-group col-md-4">
              <label htmlFor="firstname">First Name</label>
              <input type="text" className="form-control" id="firstname" placeholder="First Name" />
            </div>
            <div className="form-group col-md-4">
              <label htmlFor="lastname">Last Name</label>
              <input type="text" className="form-control" id="lastname" placeholder="Last Name" />
            </div>
            <div className="form-group col-md-4">
              <label htmlFor="phone">Phone Number</label>
              <input type="text" className="form-control" id="phone" placeholder="Phone Number" />
            </div>
          </div>

        </form>
      </Modal>

    </div>
    );
  }
}

export default Detail;
