import React,{Component} from 'react';
import { connect } from 'dva'
import { Modal, Select, DatePicker, Button, Spin, Popconfirm } from 'antd'
import Table from '../../table'

import request from 'superagent'
import config from '../../../utils/config'


const Option = Select.Option;

class Detail extends Component {
  constructor(props){
    super(props);

    let fixtureColumns = [
      {
        Header: 'Match ID',
        accessor: 'id',
        key: 'id',
        maxWidth:80
      }, {
        Header: 'Team A',
        accessor: 'first',
        filterable:false,
        key: 'first',
      }, {
        Header: 'Team B',
        accessor: 'second',
        filterable:false,
        key: 'second',
      }, {
        Header: 'Date',
        accessor: 'date',
        filterable:false,
        key: 'date',
      }, {
        Header: 'Status',
        accessor: 'status',
        filterable:false,
        key: 'status',
      }, {
        Header: 'Action',
        accessor: 'id',
        key: 'id',
        filterable:false,
        Cell: props=> (

            <Popconfirm
                title="Are you sure delete this Match?"
                onConfirm={() => this.removeMatch(props.value)}
                okText="Yes"
                cancelText="No"
              >
                <Button type="danger" shape="circle" icon="close" />
              </Popconfirm>
            ),
        maxWidth:75
      },
    ]

    let gameweekId = this.props.match.params.id
    this.state = {
      gameweekId,
      allTeams:[],
      fixtureModal:false,
      playerModal:false,
      fixtureData:{},
      fixtureColumns
    };
  }

  componentDidMount(){
    this.getFixture()
    this.getTeams()
  }

  addFixture(){
    let team1_id = this.refs.selectedTeamA.rcSelect.state.value.pop()
    let team2_id = this.refs.selectedTeamB.rcSelect.state.value.pop()
    let date = this.refs.matchDate.picker.state.value
    let status = this.refs.matchStatus.rcSelect.state.value.pop()

    console.log(team1_id, team2_id, date, status)

    request
    .post(config.apiURL+'/tournament/v1/matches/')
    .send({gameweek_id:this.state.gameweekId, team1_id, team2_id, date, status })
    .then((result) => {
      let res = JSON.parse(result.text)
      if (res.status) {
        this.getFixture()
        this.setState({ fixtureModal:false })
      }
    })

  }

  getFixture(){
    // /tournament/v1/gameweek?tournamentId=96&query=&gameweekId=3
    request
    .get(config.apiURL+'/tournament/v1/gameweek?tournamentId=96&query=&gameweekId='+this.state.gameweekId)
    .then(result => {
      let res = JSON.parse(result.text)
      if (res.status) {
        console.log(res)
        res.data[0].matches = [].concat( res.data[0].data.past, res.data[0].data.live, res.data[0].data.upcoming)
        this.setState({ fixtureData:res.data.pop() })
      }
    })
  }

  getTeams(){
    request
    .get(config.apiURL+'/tournament/v1/teamRegisteration/96/?form_type=approve')
    .then(result => {
      let res = JSON.parse(result.text)
      if (res.success) {
        console.log(res)
        this.setState({ allTeams:res.data })
      }
    })
  }

  removeMatch(id){
    console.log(id)
    request
    .delete(config.apiURL+'/tournament/v1/matches/?matchId='+id)
    .then((result) => {
      let res = JSON.parse(result.text)
      if (res.status) {
        this.getFixture()
      }
    })
  }


  uniqueKey(){
    return new Date().getTime() + Math.random()
  }

  render(){
    console.log('State', this.state)
    return (
    <div>
      <div className="row hdr mb-3">
        <div className="col-md-6">
          <h3>{this.state.fixtureData.name}</h3>
        </div>
        <div className="col-md-6 d-none">
          <button className="btn btn-outline-dark col-md-6 offset-md-6">Edit Gameweek</button>
        </div>
      </div>

      <div className="row">
        <div className="col-md-6">
          <h6>Fixtures </h6>
        </div>
        <div className="col-md-6">
          <button className="btn btn-outline-dark col-md-6 offset-md-6" onClick = {() => {this.setState({ fixtureModal:true })}} >Add Fixture</button>
        </div>
      </div>
      <Table key={this.uniqueKey()} columns = {this.state.fixtureColumns} data = {this.state.fixtureData.matches} />

      {/* Modals are as follows */}
      <Modal
        title="Add a Team"
        wrapClassName="vertical-center-modal"
        visible={this.state.fixtureModal}
        onOk={this.addFixture.bind(this)}
        onCancel={() => this.setState({ fixtureModal:false })}
      >
        <form>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label"><h6>Team A</h6></label>
            <div className="col-sm-8">
              <Select
                showSearch
                placeholder="Search here for Team A"
                filterOption={false}
                ref="selectedTeamA"
                style={{ width: '100%' }}
              >
                {
                  this.state.allTeams.map(d => <Option key={d.team_id}>{d.team_name}</Option>)
                }
              </Select>
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label"><h6>Team B</h6></label>
            <div className="col-sm-8">
              <Select
                showSearch
                placeholder="Search here for Team B"
                filterOption={false}
                ref="selectedTeamB"
                style={{ width: '100%' }}
              >
                {
                  this.state.allTeams.map(d => <Option key={d.team_id}>{d.team_name}</Option>)
                }
              </Select>
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label"><h6>Date</h6></label>
            <div className="col-sm-8">
              <DatePicker showTime format="YYYY-MM-DD HH:mm:ss" placeholder="Select Date" ref="matchDate" onChange={(val) => console.log(val)} />
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label"><h6>Status</h6></label>
            <div className="col-sm-8">
              <Select
                showSearch
                placeholder="Select Status"
                ref="matchStatus"
                style={{ width: '100%' }}
                optionFilterProp="children"
                onChange={(val) => console.log(val)}
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                >
                <Option value="live">Live</Option>
                <Option value="upcoming">Upcoming</Option>
                <Option value="past">Past</Option>
              </Select>
            </div>
          </div>


        </form>
      </Modal>

    </div>
    );
  }
}

export default Detail;
