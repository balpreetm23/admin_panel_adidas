import React,{Component} from 'react';
import PropTypes from 'prop-types'
import { routerRedux } from 'dva/router'
import { connect } from 'dva'
import { Button, Modal, DatePicker, Select } from 'antd'
import { Page } from 'components'
import queryString from 'query-string'
// import Modal from './Modal'
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import Table from '../table'

import request from 'superagent'

const Option = Select.Option;
import config from '../../utils/config'

const columns = [
   {
    Header: 'Name',
    accessor: 'name',
    key: 'name',
    filterMethod: (filter, row) =>
              row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
  }, {
    Header: 'Phone',
    accessor: 'phone',
    key: 'phone',
    filterMethod: (filter, row) =>
              row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
  }, {
    Header: 'Email',
    accessor: 'email',
    key: 'email',
    filterMethod: (filter, row) =>
              row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
  }, {
    Header: 'City/State',
    accessor: 'address',
    key: 'address',
    filterMethod: (filter, row) =>
              row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
  }, {
    Header: 'SignUp Date',
    accessor: 'createTime',
    filterable:false,
    key: 'createTime',
  }, {
    Header: 'Operation',
    accessor: 'operation',
    filterable:false,
    width: 100,
    Cell: (text, record) => {
      return <DropOption onMenuClick={e => handleMenuClick(record, e)} menuOptions={[{ key: '1', name: 'Update' }, { key: '2', name: 'Delete' }]} />
    },
  },
]


class User extends Component {
  constructor(props){
    super(props);

    this.state = {modalVisible:false};
  }

  componentDidMount(){
    this.getUsers()
  }

  getUsers(){
    request
    .get(config.apiURL+'/admin/usermanagementlist/')
    .then(result => {
      let res = JSON.parse(result.text)
      if (res.success) {
        console.log('Users', res)
        // this.setState({ tournamentStatistics:res })
      }
    })
  }

  handleAccount(event) {
    this.setState({ modalVisible:false })
    event.preventDefault()
  }

  render(){
    return (
    <div>
      <div>
        <Button icon="user-add" size={'large'} onClick = {() => this.setState({ modalVisible:true })}>Create</Button>
      </div>
      <Modal
        title="Add Account"
        wrapClassName="vertical-center-modal"
        visible={this.state.modalVisible}
        onOk={() => this.handleAccount.bind(this)}
        onCancel={() => this.setState({ modalVisible:false })}
      >
        <form>
          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="firstname">First Name</label>
              <input type="text" className="form-control" id="firstname" placeholder="First Name" />
            </div>
            <div className="form-group col-md-6">
              <label htmlFor="lastname">Last Name</label>
              <input type="text" className="form-control" id="lastname" placeholder="Last Name" />
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="inputEmail">Email</label>
            <input type="email" className="form-control" id="inputEmail" placeholder="Email" />
          </div>
          <div className="form-group">
            <label htmlFor="inputAddress">City/State </label>
            <input type="text" className="form-control" id="inputAddress" placeholder="City/State" />
          </div>
          <div className="form-group">
            <label htmlFor="inputSignupDate">Signup Date </label>
            <br/>
            <DatePicker id="inputSignupDate" size={'large'} placeholder="Select Signup Date" onChange={(val) => console.log(val)} />
          </div>
          <div className="form-group">
            <div className="input-group mb-3">
              <div className="input-group-prepend">
                <span className="input-group-text">Upload</span>
              </div>
              <div className="custom-file">
                <input type="file" className="custom-file-input" id="inputGroupFile01" />
                <label className="custom-file-label" htmlFor="inputGroupFile01">Choose file</label>
              </div>
            </div>
          </div>
          <div className="form-group">
            <Select
              showSearch
              placeholder="Select Team"
              optionFilterProp="children"
              onChange={(val) => console.log(val)}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              <Option value="jack">Jack</Option>
              <Option value="lucy">Lucy</Option>
              <Option value="tom">Tom</Option>
            </Select>

          </div>
        </form>
      </Modal>


      <Table columns = {columns} data = {this.state.data} />
    </div>
    );
  }
}

User.propTypes = {
  user: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
  loading: PropTypes.object,
}

export default connect(({ user, loading }) => ({ user, loading }))(User)
