import React,{Component} from 'react';
import { Switch, Select, Modal, DatePicker, TimePicker } from 'antd'
import Table from '../table'
import moment from 'moment';

const Option = Select.Option;

const teamPlayingColumns = [
   {
    Header: 'Select',
    accessor: 'Select',
    key: 'select',
  }, {
    Header: 'Player Name',
    accessor: 'start_date',
    filterable:false,
    key: 'start_date',
    filterMethod: (filter, row) =>
      row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
  }, {
    Header: 'J No',
    accessor: 'team',
    filterable:false,
    key: 'team',
  }, {
    Header: 'GK',
    accessor: 'city',
    key: 'city',
    filterMethod: (filter, row) =>
              row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
  },
]
const goalColumns = [
   {
    Header: 'J no',
    accessor: 'Select',
    key: 'select',
  }, {
    Header: 'Player Name',
    accessor: 'start_date',
    filterable:false,
    key: 'start_date',
    filterMethod: (filter, row) =>
      row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
  }, {
    Header: 'Team',
    accessor: 'team',
    filterable:false,
    key: 'team',
  }, {
    Header: 'Goal Type',
    accessor: 'city',
    key: 'city',
    filterMethod: (filter, row) =>
              row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
  }, {
    Header: 'Assist',
    accessor: 'city',
    key: 'city',
    filterMethod: (filter, row) =>
              row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
  }, {
    Header: 'Time',
    accessor: 'city',
    key: 'city',
    filterMethod: (filter, row) =>
              row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
  }, {
    Header: 'Action',
    accessor: 'city',
    key: 'city',
    filterMethod: (filter, row) =>
              row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
  },
]
const foulsColumns = [
   {
    Header: 'J no',
    accessor: 'Select',
    key: 'select',
  }, {
    Header: 'Player Name',
    accessor: 'start_date',
    filterable:false,
    key: 'start_date',
    filterMethod: (filter, row) =>
      row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
  }, {
    Header: 'Team',
    accessor: 'team',
    filterable:false,
    key: 'team',
  }, {
    Header: 'Foul Type',
    accessor: 'city',
    key: 'city',
    filterMethod: (filter, row) =>
              row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
  }, {
    Header: 'Assist',
    accessor: 'city',
    key: 'city',
    filterMethod: (filter, row) =>
              row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
  }, {
    Header: 'Time',
    accessor: 'city',
    key: 'city',
    filterMethod: (filter, row) =>
              row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
  }, {
    Header: 'Action',
    accessor: 'city',
    key: 'city',
    filterMethod: (filter, row) =>
              row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
  },
]
const substitutionColumns = [
   {
    Header: 'J no',
    accessor: 'Select',
    key: 'select',
  }, {
    Header: 'Player Name',
    accessor: 'start_date',
    filterable:false,
    key: 'start_date',
    filterMethod: (filter, row) =>
      row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
  }, {
    Header: 'Team',
    accessor: 'team',
    filterable:false,
    key: 'team',
  }, {
    Header: 'Substitute With',
    accessor: 'city',
    key: 'city',
    filterMethod: (filter, row) =>
              row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
  }, {
    Header: 'Time',
    accessor: 'city',
    key: 'city',
    filterMethod: (filter, row) =>
              row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
  }, {
    Header: 'Action',
    accessor: 'city',
    key: 'city',
    filterMethod: (filter, row) =>
              row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
  },
]
const penaltyColumns = [
   {
    Header: 'J no',
    accessor: 'Select',
    key: 'select',
  }, {
    Header: 'Player Name',
    accessor: 'start_date',
    filterable:false,
    key: 'start_date',
    filterMethod: (filter, row) =>
      row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
  }, {
    Header: 'Team',
    accessor: 'team',
    filterable:false,
    key: 'team',
  }, {
    Header: 'GoalKeeper',
    accessor: 'city',
    key: 'city',
    filterMethod: (filter, row) =>
              row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
  }, {
    Header: 'Goal',
    accessor: 'city',
    key: 'city',
    filterMethod: (filter, row) =>
              row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
  }, {
    Header: 'Action',
    accessor: 'city',
    key: 'city',
    filterMethod: (filter, row) =>
              row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
  },
]
const teamStatsColumns = [
   {
    Header: 'J no',
    accessor: 'Select',
    key: 'select',
  }, {
    Header: 'Player Name',
    accessor: 'start_date',
    filterable:false,
    key: 'start_date',
    filterMethod: (filter, row) =>
      row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
  }, {
    Header: 'Shots',
    accessor: 'team',
    filterable:false,
    key: 'team',
  }, {
    Header: 'Shots on T',
    accessor: 'team',
    filterable:false,
    key: 'team',
  }, {
    Header: 'Corners',
    accessor: 'team',
    filterable:false,
    key: 'team',
  }, {
    Header: 'Fouls',
    accessor: 'team',
    filterable:false,
    key: 'team',
  }, {
    Header: 'Woodwork',
    accessor: 'team',
    filterable:false,
    key: 'team',
  }, {
    Header: 'Interception',
    accessor: 'team',
    filterable:false,
    key: 'team',
  }, {
    Header: 'Shots Blocked',
    accessor: 'team',
    filterable:false,
    key: 'team',
  }, {
    Header: 'Saves',
    accessor: 'city',
    key: 'city',
    filterMethod: (filter, row) =>
              row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
  },
]


class Scoring extends Component {

  constructor(props){
    super(props);

    this.state = {
      teamAData:[],
      teamBData:[],
      goalModal:false,
      foulsModal:false,
      substitutionModal:false,
      penaltyModal:false,
    };
  }

  render(){
    return (
    <div>
      <div className="row  mb-3">
        <div className="col-md-6">
          <h3>T1 vs T2</h3>
        </div>
        <div className="col-md-6">
          <button className="btn btn-outline-dark col-md-6 offset-md-6">Edit Fixture</button>
        </div>
      </div>
      <h6>Gameweek 1, 9th April 2018, The Dome, New Delhi</h6>
      <form>
        <div className="form-group row mt-3">
          <label className="col-sm-2 col-form-label"><h6>Match Status</h6></label>
          <div className="col-sm-2">
            <Select
              showSearch
              placeholder="LIVE"
              style={{ width: '100%' }}
              optionFilterProp="children"
              onChange={(val) => console.log(val)}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              >
              <Option value="jack">Jack</Option>
              <Option value="lucy">Lucy</Option>
              <Option value="tom">Tom</Option>
            </Select>
          </div>
        </div>
        <div className="row">
          <div className="col-md-9">
            <div className="form-group row mt-3">
              <label className="col-sm-2 col-form-label"><h6>Half Time</h6></label>
              <div className="col-sm-1 mt-1">
                <Switch defaultChecked />
              </div>
              <div className="col-sm-2">
                <Select
                  showSearch
                  placeholder="Extra Minutes"
                  style={{ width: '100%' }}
                  optionFilterProp="children"
                  onChange={(val) => console.log(val)}
                  filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                  >
                  <Option value="jack">Jack</Option>
                  <Option value="lucy">Lucy</Option>
                  <Option value="tom">Tom</Option>
                </Select>
              </div>
            </div>
            <div className="form-group row mt-3">
              <label className="col-sm-2 col-form-label"><h6>Full Time</h6></label>
              <div className="col-sm-1 mt-1">
                <Switch defaultChecked />
              </div>
              <div className="col-sm-2">
                <Select
                  showSearch
                  placeholder="Extra Minutes"
                  style={{ width: '100%' }}
                  optionFilterProp="children"
                  onChange={(val) => console.log(val)}
                  filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                  >
                  <Option value="jack">Jack</Option>
                  <Option value="lucy">Lucy</Option>
                  <Option value="tom">Tom</Option>
                </Select>
              </div>
            </div>
          </div>
          <div className="col-md-3">
            <div className="form-group row mt-3">
              <label className="col-sm-6 col-form-label"><h6>Extra Time</h6></label>
              <div className="col-sm-1 mt-1">
                <Switch defaultChecked />
              </div>
            </div>
            <div className="form-group row mt-3">
              <label className="col-sm-6 col-form-label"><h6>Penalties</h6></label>
              <div className="col-sm-1 mt-1">
                <Switch defaultChecked />
              </div>
            </div>
          </div>
        </div>
        <div className="form-group row mt-3">
          <label className="col-sm-2 col-form-label"><h6>Winner</h6></label>
          <div className="col-sm-2">
            <Select
              showSearch
              placeholder="Team A"
              style={{ width: '100%' }}
              optionFilterProp="children"
              onChange={(val) => console.log(val)}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              >
              <Option value="jack">Jack</Option>
              <Option value="lucy">Lucy</Option>
              <Option value="tom">Tom</Option>
            </Select>
          </div>
        </div>

      </form>
      <hr/>
      <div className="row">
        <div className="col-md-6">
          <h5 className="mt-3 mb-3 text-center">Team 1 Playing 11</h5>
          <Table columns = {teamPlayingColumns} data = {this.state.teamAData} />
        </div>
        <div className="col-md-6">
          <h5 className="mt-3 mb-3 text-center">Team 2 Playing 11</h5>
          <Table columns = {teamPlayingColumns} data = {this.state.teamBData} />
        </div>
      </div>
      <div className="row mt-3 mb-3">
        <div className="col-md-6">
          <h4 className="mt-2"> Goals </h4>
        </div>
        <div className="col-md-6">
          <button className="btn btn-outline-dark col-md-6 offset-md-6" onClick = {() => {this.setState({ goalModal:true })}} >Add Goal</button>
        </div>
      </div>

      <Table columns = {goalColumns} data = {this.state.goalData} />
      <div className="row mt-3 mb-3">
        <div className="col-md-6">
          <h4 className="mt-2"> Fouls </h4>
        </div>
        <div className="col-md-6">
          <button className="btn btn-outline-dark col-md-6 offset-md-6" onClick = {() => {this.setState({ foulsModal:true })}} >Add Foul</button>
        </div>
      </div>

      <Table columns = {foulsColumns} data = {this.state.foulsData} />
      <div className="row mt-3 mb-3">
        <div className="col-md-6">
          <h4 className="mt-2"> Substitution </h4>
        </div>
        <div className="col-md-6">
          <button className="btn btn-outline-dark col-md-6 offset-md-6" onClick = {() => {this.setState({ substitutionModal:true })}} >Add Substitution</button>
        </div>
      </div>

      <Table columns = {substitutionColumns} data = {this.state.substitutionData} />
      <div className="row mt-3 mb-3">
        <div className="col-md-6">
          <h4 className="mt-2"> Team A Other Stats </h4>
        </div>
        <div className="col-md-6">
          <button className="btn btn-outline-dark col-md-6 offset-md-6" onClick = {() => console.log('clicked')} >Update</button>
        </div>
      </div>

      <Table columns = {teamStatsColumns} data = {this.state.teamAStatsData} />
      <div className="row mt-3 mb-3">
        <div className="col-md-6">
          <h4 className="mt-2"> Team B Other Stats </h4>
        </div>
        <div className="col-md-6">
          <button className="btn btn-outline-dark col-md-6 offset-md-6" onClick = {() => console.log('clicked')} >Update</button>
        </div>
      </div>

      <Table columns = {teamStatsColumns} data = {this.state.teamBStatsData} />
      <div className="row mt-3 mb-3">
        <div className="col-md-6">
          <h4 className="mt-2"> Extra Time Penalties </h4>
        </div>
        <div className="col-md-6">
          <button className="btn btn-outline-dark col-md-6 offset-md-6" onClick = {() => {this.setState({ penaltyModal:true })}} >Add Penalty</button>
        </div>
      </div>

      <Table columns = {penaltyColumns} data = {this.state.penaltyData} />
        {/* Modals are as follows */}
      <Modal
        title="Add a Goal"
        wrapClassName="vertical-center-modal"
        visible={this.state.goalModal}
        onOk={() => this.addOwner.bind(this)}
        onCancel={() => this.setState({ goalModal:false })}
      >
        <form>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label"><h6>Team</h6></label>
            <div className="col-sm-8">
              <Select
                showSearch
                placeholder="Select Team"
                style={{ width: '100%' }}
                optionFilterProp="children"
                onChange={(val) => console.log(val)}
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                >
                <Option value="jack">Jack</Option>
                <Option value="lucy">Lucy</Option>
                <Option value="tom">Tom</Option>
              </Select>
            </div>
          </div>

          <div className="form-group row">
            <label className="col-sm-4 col-form-label"><h6>Player</h6></label>
            <div className="col-sm-8">
              <Select
                showSearch
                placeholder="Select Player"
                style={{ width: '100%' }}
                optionFilterProp="children"
                onChange={(val) => console.log(val)}
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                >
                <Option value="jack">Jack</Option>
                <Option value="lucy">Lucy</Option>
                <Option value="tom">Tom</Option>
              </Select>
            </div>
          </div>

          <div className="form-group row">
            <label className="col-sm-4 col-form-label"><h6>Goal Type</h6></label>
            <div className="col-sm-8">
              <Select
                showSearch
                placeholder="Select Goal Type"
                style={{ width: '100%' }}
                optionFilterProp="children"
                onChange={(val) => console.log(val)}
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                >
                <Option value="jack">Jack</Option>
                <Option value="lucy">Lucy</Option>
                <Option value="tom">Tom</Option>
              </Select>
            </div>
          </div>

          <div className="form-group row">
            <label className="col-sm-4 col-form-label"><h6>Assist </h6></label>
            <div className="col-sm-8">
              <Select
                showSearch
                placeholder="Select Assist"
                style={{ width: '100%' }}
                optionFilterProp="children"
                onChange={(val) => console.log(val)}
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                >
                <Option value="jack">Jack</Option>
                <Option value="lucy">Lucy</Option>
                <Option value="tom">Tom</Option>
              </Select>
            </div>
          </div>

          <div className="form-group row">
            <label className="col-sm-4 col-form-label"><h6>Time </h6></label>
            <div className="col-sm-8">
              <TimePicker defaultOpenValue={moment('00:00:00', 'HH:mm:ss')} />,
            </div>
          </div>
        </form>
      </Modal>

      <Modal
        title="Add a Foul"
        wrapClassName="vertical-center-modal"
        visible={this.state.foulsModal}
        onOk={() => this.addOwner.bind(this)}
        onCancel={() => this.setState({ foulsModal:false })}
      >
        <form>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label"><h6>Team</h6></label>
            <div className="col-sm-8">
              <Select
                showSearch
                placeholder="Select Team"
                style={{ width: '100%' }}
                optionFilterProp="children"
                onChange={(val) => console.log(val)}
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                >
                <Option value="jack">Jack</Option>
                <Option value="lucy">Lucy</Option>
                <Option value="tom">Tom</Option>
              </Select>
            </div>
          </div>

          <div className="form-group row">
            <label className="col-sm-4 col-form-label"><h6>Player</h6></label>
            <div className="col-sm-8">
              <Select
                showSearch
                placeholder="Select Player"
                style={{ width: '100%' }}
                optionFilterProp="children"
                onChange={(val) => console.log(val)}
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                >
                <Option value="jack">Jack</Option>
                <Option value="lucy">Lucy</Option>
                <Option value="tom">Tom</Option>
              </Select>
            </div>
          </div>

          <div className="form-group row">
            <label className="col-sm-4 col-form-label"><h6>Foul Type</h6></label>
            <div className="col-sm-8">
              <Select
                showSearch
                placeholder="Select Foul Type"
                style={{ width: '100%' }}
                optionFilterProp="children"
                onChange={(val) => console.log(val)}
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                >
                <Option value="jack">Jack</Option>
                <Option value="lucy">Lucy</Option>
                <Option value="tom">Tom</Option>
              </Select>
            </div>
          </div>

          <div className="form-group row">
            <label className="col-sm-4 col-form-label"><h6>Foul on </h6></label>
            <div className="col-sm-8">
              <Select
                showSearch
                placeholder="Select Foul on"
                style={{ width: '100%' }}
                optionFilterProp="children"
                onChange={(val) => console.log(val)}
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                >
                <Option value="jack">Jack</Option>
                <Option value="lucy">Lucy</Option>
                <Option value="tom">Tom</Option>
              </Select>
            </div>
          </div>

          <div className="form-group row">
            <label className="col-sm-4 col-form-label"><h6>Time </h6></label>
            <div className="col-sm-8">
              <TimePicker defaultOpenValue={moment('00:00:00', 'HH:mm:ss')} />,
            </div>
          </div>
        </form>
      </Modal>

      <Modal
        title="Add a Substitution"
        wrapClassName="vertical-center-modal"
        visible={this.state.substitutionModal}
        onOk={() => this.addOwner.bind(this)}
        onCancel={() => this.setState({ substitutionModal:false })}
      >
        <form>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label"><h6>Team</h6></label>
            <div className="col-sm-8">
              <Select
                showSearch
                placeholder="Select Team"
                style={{ width: '100%' }}
                optionFilterProp="children"
                onChange={(val) => console.log(val)}
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                >
                <Option value="jack">Jack</Option>
                <Option value="lucy">Lucy</Option>
                <Option value="tom">Tom</Option>
              </Select>
            </div>
          </div>

          <div className="form-group row">
            <label className="col-sm-4 col-form-label"><h6>Player</h6></label>
            <div className="col-sm-8">
              <Select
                showSearch
                placeholder="Select Player"
                style={{ width: '100%' }}
                optionFilterProp="children"
                onChange={(val) => console.log(val)}
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                >
                <Option value="jack">Jack</Option>
                <Option value="lucy">Lucy</Option>
                <Option value="tom">Tom</Option>
              </Select>
            </div>
          </div>

          <div className="form-group row">
            <label className="col-sm-4 col-form-label"><h6>Substituted With</h6></label>
            <div className="col-sm-8">
              <Select
                showSearch
                placeholder="Select Substituted With"
                style={{ width: '100%' }}
                optionFilterProp="children"
                onChange={(val) => console.log(val)}
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                >
                <Option value="jack">Jack</Option>
                <Option value="lucy">Lucy</Option>
                <Option value="tom">Tom</Option>
              </Select>
            </div>
          </div>

          <div className="form-group row">
            <label className="col-sm-4 col-form-label"><h6>Time </h6></label>
            <div className="col-sm-8">
              <TimePicker defaultOpenValue={moment('00:00:00', 'HH:mm:ss')} />,
            </div>
          </div>
        </form>
      </Modal>

      <Modal
        title="Add a Penalty"
        wrapClassName="vertical-center-modal"
        visible={this.state.penaltyModal}
        onOk={() => this.addOwner.bind(this)}
        onCancel={() => this.setState({ penaltyModal:false })}
      >
        <form>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label"><h6>Team</h6></label>
            <div className="col-sm-8">
              <Select
                showSearch
                placeholder="Select Team"
                style={{ width: '100%' }}
                optionFilterProp="children"
                onChange={(val) => console.log(val)}
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                >
                <Option value="jack">Jack</Option>
                <Option value="lucy">Lucy</Option>
                <Option value="tom">Tom</Option>
              </Select>
            </div>
          </div>

          <div className="form-group row">
            <label className="col-sm-4 col-form-label"><h6>Player</h6></label>
            <div className="col-sm-8">
              <Select
                showSearch
                placeholder="Select Player"
                style={{ width: '100%' }}
                optionFilterProp="children"
                onChange={(val) => console.log(val)}
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                >
                <Option value="jack">Jack</Option>
                <Option value="lucy">Lucy</Option>
                <Option value="tom">Tom</Option>
              </Select>
            </div>
          </div>

          <div className="form-group row">
            <label className="col-sm-4 col-form-label"><h6>Goalkeeper</h6></label>
            <div className="col-sm-8">
              <Select
                showSearch
                placeholder="Select Goalkeeper"
                style={{ width: '100%' }}
                optionFilterProp="children"
                onChange={(val) => console.log(val)}
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                >
                <Option value="jack">Jack</Option>
                <Option value="lucy">Lucy</Option>
                <Option value="tom">Tom</Option>
              </Select>
            </div>
          </div>

          <div className="form-group row">
            <label className="col-sm-4 col-form-label"><h6>Goal </h6></label>
            <div className="col-sm-8">
              <Switch />
            </div>
          </div>
        </form>
      </Modal>


    </div>
    );
  }
}

export default Scoring;
