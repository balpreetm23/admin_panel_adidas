const { config } = require('./common')

const { apiPrefix } = config
let database = [
  {
    id: '4',
    name: 'Accounts',
    route: '/user',
  },
  {
    id: '3',
    name: 'Tournaments',
    route: '/tournaments',
  },
  {
    id: '21',
    mpid: '-1',
    bpid: '3',
    name: 'Tournaments Detail',
    route: '/tournaments/:id',
  },
  {
    id: '22',
    mpid: '-1',
    bpid: '3',
    name: 'Teams Detail',
    route: '/teams/:id',
  },
  {
    id: '23',
    mpid: '-1',
    bpid: '3',
    name: 'Gameweek Detail',
    route: '/gameweek/:id',
  },
  {
    id: '2',
    name: 'Content Management',
    route: '/contentmanagement',
  },
  {
    id: '5',
    name: 'Scoring',
    route: '/scoring',
  },
]

module.exports = {

  [`GET ${apiPrefix}/menus`] (req, res) {
    res.status(200).json(database)
  },
}
