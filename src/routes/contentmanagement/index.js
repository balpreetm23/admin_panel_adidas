import React,{Component} from 'react';
import Table from '../table'
import { Modal, Select, DatePicker, Switch, Button, Popconfirm } from 'antd'
import request from 'superagent'
import config from '../../utils/config'


class Content extends Component {
  constructor(props){
    super(props);

    let imagesColumns = [
      {
        Header: 'S.no',
        accessor: 'sno',
        filterable:false,
        key: 'sno',
        maxWidth:50
      }, {
        Header: 'Match ID',
        accessor: 'matchId',
        key: 'matchId',
        maxWidth:100
      }, {
        Header: 'Post',
        accessor: 'heading',
        key: 'heading',
      }, {
        Header: 'Date Posted',
        accessor: 'date',
        filterable:false,
        key: 'date',
        maxWidth:120,
      }, {
        Header: 'Pushed On',
        accessor: 'pushedOn',
        filterable:false,
        key: 'pushedOn',
      }, {
        Header: 'Action',
        accessor: 'id',
        key: 'id',
        filterable:false,
        Cell: props=> (
          <Popconfirm
              title="Are you sure delete this Media?"
              onConfirm= {() => this.removeMedia(props.value)}
              okText="Yes"
              cancelText="No"
            >
              <Button type="danger" shape="circle" icon="close" />
            </Popconfirm>
        ),
        maxWidth:75
      },
    ]


    let videosColumns = [
      {
        Header: 'S.no',
        accessor: 'sno',
        filterable:false,
        key: 'sno',
        maxWidth:50
      }, {
        Header: 'Match ID',
        accessor: 'matchId',
        key: 'matchId',
        maxWidth:100
      }, {
        Header: 'Post',
        accessor: 'heading',
        key: 'heading',
      }, {
        Header: 'Date Posted',
        accessor: 'date',
        filterable:false,
        key: 'date',
        maxWidth:120,
      }, {
        Header: 'Pushed On',
        accessor: 'pushedOn',
        filterable:false,
        key: 'pushedOn',
      }, {
        Header: 'Action',
        accessor: 'id',
        key: 'id',
        filterable:false,
        Cell: props=> (
          <Popconfirm
              title="Are you sure delete this Media?"
              onConfirm= {() => this.removeMedia(props.value)}
              okText="Yes"
              cancelText="No"
            >
              <Button type="danger" shape="circle" icon="close" />
            </Popconfirm>
        ),
        maxWidth:75
      },
    ]


    let blogColumns = [
      {
        Header: 'S.no',
        accessor: 'sno',
        filterable:false,
        key: 'sno',
        maxWidth:50
      }, {
        Header: 'Match ID',
        accessor: 'matchId',
        key: 'matchId',
        maxWidth:100
      }, {
        Header: 'Post',
        accessor: 'heading',
        key: 'heading',
      }, {
        Header: 'Date Posted',
        accessor: 'date',
        filterable:false,
        key: 'date',
        maxWidth:120,
      }, {
        Header: 'Pushed On',
        accessor: 'pushedOn',
        filterable:false,
        key: 'pushedOn',
      }, {
        Header: 'Action',
        accessor: 'id',
        key: 'id',
        filterable:false,
        Cell: props=> (
          <Popconfirm
              title="Are you sure delete this Media?"
              onConfirm= {() => this.removeMedia(props.value)}
              okText="Yes"
              cancelText="No"
            >
              <Button type="danger" shape="circle" icon="close" />
            </Popconfirm>
        ),
        maxWidth:75
      },
    ]

    console.log(config.apiURL)
    this.state = {
      imagesModal:false,
      videosModal:false,
      blogModal:false,
      teams:{},
      mediaData:{},
      uploadImagesData:{},
      uploadVideosData:{},
      uploadBlogsData:{},
      loading:false,
      imagesColumns,
      videosColumns,
      blogColumns
    };
  }

  componentDidMount(){
    this.getData()
  }

  removeMedia(id){
    console.log(id)

    request
    .delete(config.apiURL+'/tournament/v1/media/?id='+id)
    .then((result) => {
      let res = JSON.parse(result.text)
      if (res.status) {
        this.getData()
      }
    })
  }

  submitForm(event, type, title, matchId, media){
    console.log(title, matchId, media)
    this.getTeams(matchId)
    .then(() => document.getElementById(type).classList.toggle('d-none'))

    event.preventDefault()
  }

  uploadImages(){

    this.setState({ loading:true }, () => {

      console.log('upload', this.state.uploadImagesData)
      let data = this.state.uploadImagesData
      let images = this.refs.images.files
      let imgrqst = request
      .post(config.apiURL + '/tournament/v1/media/')
      .field('type','images')
      .field('data', JSON.stringify(data))
      // this.state.uploadImagesData.images.map(val => /)
      Object.values(images).map(image => {
        imgrqst.attach('images', image)
      })
      imgrqst.then(result => {
        let res = JSON.parse(result.text)
        console.log(res)
        if (res.status) {
          this.setState({ imagesModal:false, loading:false }),
          this.getData()
        }
      })
    })
  }

  uploadVideos(){
    console.log('upload', this.state.uploadVideosData)
    let data = this.state.uploadVideosData

    request
    .post(config.apiURL + '/tournament/v1/media/')
    .field('type','video')
    .field('data', JSON.stringify(data))
    .field('video_link', this.refs.videoLink.value)
    .then(result => {
      let res = JSON.parse(result.text)
      console.log(res)
      if (res.status) {
        this.setState({ videosModal:false })
        this.getData()
      }
    })
  }

  uploadBlog(){
    let data = this.state.uploadBlogsData
    let blog_link = this.refs.blog_link.value
    let blog_introduction = this.refs.blog_introduction.value
    let blog_preview_image_link = this.refs.blog_preview_image_link.files[0]
    console.log('blog', this.state.uploadBlogsData, blog_link, blog_introduction, blog_preview_image_link)
    request
    .post(config.apiURL + '/tournament/v1/media/')
    .field('type','blog')
    .field('data', JSON.stringify(data))
    .field('blog_link', blog_link)
    .field('blog_introduction', blog_introduction)
    .attach('blog_preview_image_link', blog_preview_image_link)
    .then(result => {
      let res = JSON.parse(result.text)
      console.log(res)
      if (res.status) {
        this.setState({ blogModal:false })
        this.getData()
      }
    })

  }

  getTeams(matchId){
    return request
    .get(config.apiURL + '/tournament/v1/matches?matchId=' + matchId)
    .then(result => {
      let res = JSON.parse(result.text)
      console.log(res)
      if (res.status) {
        this.setState({ teams:res.data })
      }
    })
  }

  getData(){
    request
    .post(config.apiURL + '/tournament/v1/media/')
    .field('type','getData')
    .then(result => {
      let res = JSON.parse(result.text)
      console.log(res)
      if (res.status) {
        this.setState({ mediaData:res.data })
      }
    })
  }

  uniqueKey(){
    return new Date().getTime() + Math.random()
  }

  updateFieldImage( field, value ){
    let uploadImagesData = this.state.uploadImagesData;
    uploadImagesData[field] = value
    this.setState({ uploadImagesData })
  }

  updateFieldVideo( field, value ){
    let uploadVideosData = this.state.uploadVideosData;
    uploadVideosData[field] = value
    this.setState({ uploadVideosData })
  }

  updateFieldBlog( field, value ){
    let uploadBlogsData = this.state.uploadBlogsData;
    uploadBlogsData[field] = value
    this.setState({ uploadBlogsData })
  }


  render(){
    console.log('Content Management', this.state)
    return (
    <div>
      <div className="row hdr mb-3">
        <div className="col-md-6">
          <h3>Content Management</h3>
        </div>
        <div className="col-md-6">
          <button className="btn btn-outline-dark col-md-6 offset-md-6">Full List</button>
        </div>
      </div>

      <div className="row mb-3 mt-3">
        <div className="col-md-6">
          <h6>Upload Images</h6>
        </div>
        <div className="col-md-6">
          <button className="btn btn-outline-dark col-md-6 offset-md-6" onClick = {() => {this.setState({ imagesModal:true })}} >Upload Images</button>
        </div>
      </div>
      <Table key = {this.uniqueKey()} columns = {this.state.imagesColumns} data = {this.state.mediaData.images} />

      <div className="row mb-3 mt-3">
        <div className="col-md-6">
          <h6>Upload Videos</h6>
        </div>
        <div className="col-md-6">
          <button className="btn btn-outline-dark col-md-6 offset-md-6" onClick = {() => {this.setState({ videosModal:true })}} >Upload Videos</button>
        </div>
      </div>
      <Table key = {this.uniqueKey()} columns = {this.state.videosColumns} data = { this.state.mediaData.videos} />

      <div className="row mb-3 mt-3">
        <div className="col-md-6">
          <h6>Upload Blog</h6>
        </div>
        <div className="col-md-6">
          <button className="btn btn-outline-dark col-md-6 offset-md-6" onClick = {() => {this.setState({ blogModal:true })}} >Upload Blog</button>
        </div>
      </div>
      <Table key = {this.uniqueKey()} columns = {this.state.blogColumns} data = {this.state.mediaData.blogs} />

      <Modal
        title="Upload Images"
        wrapClassName="vertical-center-modal"
        visible={this.state.imagesModal}
        width = {900}
        confirmLoading={this.state.loading}
        onOk={this.uploadImages.bind(this)}
        onCancel={() => this.setState({ imagesModal:false, uploadImagesData:{} })}
      >
        <form  onSubmit = {(event) => this.submitForm(event, 'images', this.refs.imageTitle.value, this.refs.imagesMatchId.value, this.refs.images.files)} >
          <div className="row">
            <div className="col-md-6">
              <div className="form-group row">
                <label className="col-sm-4 col-form-label"><h6>Title</h6></label>
                <div className="col-sm-8">
                  <input type="text" onChange={(e) => this.updateFieldImage('title', e.target.value)} required ref="imageTitle" className="form-control" placeholder="Title"/>
                </div>
              </div>

              <div className="form-group row">
                <label className="col-sm-4 col-form-label"><h6>Match ID</h6></label>
                <div className="col-sm-8">
                  <input type="text" onChange={(e) => this.updateFieldImage('match_id', e.target.value)} className="form-control" ref="imagesMatchId" required placeholder="Match ID"/>
                </div>
              </div>

              <div className="form-group">
                <div className="input-group mb-3">
                  <div className="">
                    <input type="file" required multiple ref="images"  id="logoUpload" />
                  </div>
                </div>
              </div>
              <div className="row">
                <button type="submit" className="btn-outline-dark btn-sm col-md-4 offset-md-8">Submit</button>
              </div>
            </div>

            <div className="col-md-6">
              <h6 className="text-center hdr">Push Images on</h6>
              <div className="row mt-2">
                <div className="col-md-10"><h6>Adidas Tournament Page</h6></div>
                <div className="col-md-2">
                  <Switch onChange={(e) => this.updateFieldImage('disp_tournament', e ? 1 : 0)}  />
                </div>
              </div>
              <div className="row ">
                <div className="col-md-10"><h6>Feed</h6></div>
                <div className="col-md-2">
                  <Switch onChange={(e) => this.updateFieldImage('disp_feed', e ? 1 : 0)}  />
                </div>
              </div>
              <div className="d-none" id="images">
                <div className="row ">
                  <div className="col-md-10"><h6>{this.state.teams.teamA && this.state.teams.teamA.name}</h6></div>
                  <div className="col-md-2">
                    <Switch onChange={(e) => this.updateFieldImage('team1_id', e && this.state.teams.teamA.id )}  />
                  </div>
                </div>
                <div className="row ">
                <div className="col-md-10"><h6>{this.state.teams.teamB && this.state.teams.teamB.name}</h6></div>
                <div className="col-md-2">
                  <Switch onChange={(e) => this.updateFieldImage('team2_id', e && this.state.teams.teamB.id )}  />
                </div>
              </div>
                <div className="row ">
                <div className="col-md-10"><h6>Match</h6></div>
                <div className="col-md-2">
                  <Switch onChange={(e) => this.updateFieldImage('disp_match', e ? 1 : 0)}  />
                </div>
              </div>
              </div>
            </div>
          </div>
        </form>
      </Modal>

      <Modal
        title="Upload Blog"
        wrapClassName="vertical-center-modal"
        visible={this.state.blogModal}
        width = {900}
        onOk={ this.uploadBlog.bind(this)}
        onCancel={() => this.setState({ blogModal:false })}
      >
        <form>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group row">
                <label className="col-sm-4 col-form-label"><h6>Title</h6></label>
                <div className="col-sm-8">
                  <input type="text" onChange={(e) => this.updateFieldBlog('title', e.target.value)} className="form-control" placeholder="Title"/>
                </div>
              </div>

              <div className="form-group">
                <div className="input-group mb-3">
                  <div className="input-group-prepend">
                    <span className="input-group-text">Upload Preview Image</span>
                  </div>
                  <div className="custom-file">
                    <input type="file" ref="blog_preview_image_link" className="custom-file-input" id="logoUpload" />
                    <label className="custom-file-label"  htmlFor="logoUpload">Choose file</label>
                  </div>
                </div>
              </div>

            <div className="form-group row">
              <label className="col-sm-4 col-form-label"><h6>Introduction</h6></label>
              <div className="col-sm-8">
                <textarea  rows="5" ref="blog_introduction" className="form-control"></textarea>
              </div>
            </div>

            <div className="form-group row">
              <label className="col-sm-4 col-form-label"><h6>Add Blog Link</h6></label>
              <div className="col-sm-8">
                <input type="text" ref="blog_link" className="form-control" placeholder="Blog Link"/>
              </div>
            </div>

          </div>
            <div className="col-md-6">
              <h6 className="text-center hdr">Push Images on</h6>
              <div className="row mt-2">
                <div className="col-md-10"><h6>Adidas Tournament Page</h6></div>
                <div className="col-md-2">
                  <Switch onChange={(e) => this.updateFieldBlog('disp_tournament', e ? 1 : 0)}   />
                </div>
              </div>
              <div className="row ">
                <div className="col-md-10"><h6>Feed</h6></div>
                <div className="col-md-2">
                  <Switch onChange={(e) => this.updateFieldBlog('disp_feed', e ? 1 : 0)}    />
                </div>
              </div>
            </div>
          </div>
        </form>
      </Modal>

      <Modal
        title="Upload Videos"
        wrapClassName="vertical-center-modal"
        visible={this.state.videosModal}
        width = {900}
        onOk={this.uploadVideos.bind(this)}
        onCancel={() => this.setState({ videosModal:false })}
      >
      <form  onSubmit = {(event) => this.submitForm(event, 'video', this.refs.videoTitle.value, this.refs.videosMatchId.value, this.refs.videoLink.value)} >
        <div className="row">
          <div className="col-md-6">
            <div className="form-group row">
              <label className="col-sm-4 col-form-label"><h6>Title</h6></label>
              <div className="col-sm-8">
                <input type="text" onChange={(e) => this.updateFieldVideo('title', e.target.value)} required ref="videoTitle" className="form-control" placeholder="Title"/>
              </div>
            </div>

            <div className="form-group row">
              <label className="col-sm-4 col-form-label"><h6>Match ID</h6></label>
              <div className="col-sm-8">
                <input type="text" onChange={(e) => this.updateFieldVideo('match_id', e.target.value)} className="form-control" ref="videosMatchId" required placeholder="Match ID"/>
              </div>
            </div>

            <div className="form-group row">
              <label className="col-sm-4 col-form-label"><h6>Add Video Link</h6></label>
              <div className="col-sm-8">
                <input type="text" ref="videoLink" className="form-control" placeholder="Video Link"/>
              </div>
            </div>
            <div className="row">
              <button type="submit" className="btn-outline-dark btn-sm col-md-4 offset-md-8">Submit</button>
            </div>
          </div>

          <div className="col-md-6">
            <h6 className="text-center hdr">Push Images on</h6>
            <div className="row mt-2">
              <div className="col-md-10"><h6>Adidas Tournament Page</h6></div>
              <div className="col-md-2">
                <Switch onChange={(e) => this.updateFieldVideo('disp_tournament', e ? 1 : 0)}  />
              </div>
            </div>
            <div className="row ">
              <div className="col-md-10"><h6>Feed</h6></div>
              <div className="col-md-2">
                <Switch onChange={(e) => this.updateFieldVideo('disp_feed', e ? 1 : 0)}  />
              </div>
            </div>
            <div className="d-none" id="video">
              <div className="row ">
                <div className="col-md-10"><h6>{this.state.teams.teamA && this.state.teams.teamA.name}</h6></div>
                <div className="col-md-2">
                  <Switch onChange={(e) => this.updateFieldVideo('team1_id', e && this.state.teams.teamA.id )}  />
                </div>
              </div>
              <div className="row ">
              <div className="col-md-10"><h6>{this.state.teams.teamB && this.state.teams.teamB.name}</h6></div>
              <div className="col-md-2">
                <Switch onChange={(e) => this.updateFieldVideo('team2_id', e && this.state.teams.teamA.id )}  />
              </div>
            </div>
              <div className="row ">
              <div className="col-md-10"><h6>Match</h6></div>
              <div className="col-md-2">
                <Switch onChange={(e) => this.updateFieldVideo('disp_match', e ? 1 : 0)}  />
              </div>
            </div>
            </div>
          </div>
        </div>
      </form>
    </Modal>




    </div>
    );
  }
}

export default Content;
