import React,{Component} from 'react';
import { connect } from 'dva'
import { Modal, Select, DatePicker, Button, Spin, Popconfirm } from 'antd'
import Table from '../../table'
import request from 'superagent'
import config from '../../../utils/config'
const Option = Select.Option;



class Detail extends Component {

  constructor(props){
    super(props);
    let tournamentId = this.props.match.params.id

  let teamsColumns = [
    {
      Header: 'Team Name',
      accessor: 'team_name',
      key: 'team_name',
      filterMethod: (filter, row) =>
      row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
      Cell: props=> (
        <div>
          <a href={'/teams/'+props.original.team_id}>{props.value}</a>
        </div>
      )
    }
  ]

  let fixtureColumns = [
    {
      Header: 'Gameweek',
      accessor: 'name',
      key: 'name',
      filterMethod: (filter, row) =>
      row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
      Cell: props=> (
        <div>
          <a href={'/gameweek/'+props.original.id}>{props.value}</a>
        </div>
      )

    }, {
      Header: 'Gameweek Begin Date',
      accessor: 'begin_date',
      filterable:false,
      key: 'begin_date',
    }, {
      Header: 'Matches',
      accessor: 'data',
      filterable:false,
      key: 'data',
      Cell: props=> (
        <div>
          {console.log(props)}
          {props.value.live.length + props.value.upcoming.length + props.value.past.length}
        </div>
      ),

    }
  ]

  let sponsorColumns = [
    {
      Header: 'Sponsor Name',
      accessor: 'name',
      key: 'name',
      filterMethod: (filter, row) =>
      row[filter.id].toLowerCase().includes(filter.value.toLowerCase()),
    }, {
      Header: 'Sponsor Image',
      accessor: 'image',
      filterable:false,
      key: 'image',
      Cell: props=> <a href={config.apiURL.replace('/api', '') + props.value} target="_blank">{config.apiURL.replace('/api', '') + props.value}</a>
  }, {
    Header: 'Action',
    accessor: 'id',
    key: 'id',
    filterable:false,
    Cell: props=> (
      <Popconfirm
          title="Are you sure delete this Sponsor?"
          onConfirm={() => this.removeSponsor(props.value)}
          okText="Yes"
          cancelText="No"
        >
          <Button type="danger" shape="circle" icon="close" />
        </Popconfirm>
    ),
    maxWidth:75

      },
    ]
    this.state = {
      tournamentId,
      tournamentData:{},
      tournamentTeams:[],
      allTeams:[],
      tournamentSponsor:[],
      tournamentFixtures:{},
      tournamentStatistics:{},
      teamModal:false,
      fixtureModal:false,
      sponsorModal:false,
      fetching:false,
      teamsColumns,
      fixtureColumns,
      sponsorColumns

    };
    console.log('config', config)
  }

  componentDidMount(){
    // Get basic info

    this.getTournamentData()
    this.getTournamentSponsor()
    this.getTeams()
    this.getFixtures()

    request
    .get(config.apiURL+'/tournament/v1/profile/?form_type=statics&tournament_id='+this.state.tournamentId)
    .then(result => {
      let res = JSON.parse(result.text)
      if (res.success) {
        console.log('Stats', res)
        this.setState({ tournamentStatistics:res })
      }
    })

  }

  getFixtures(){
    request
    .get(config.apiURL+'/tournament/v1/gameweek?tournamentId=' + this.state.tournamentId + '&query=')
    // .get(config.apiURL+'/tournament/v1/profile/?form_type=fixture&tournament_id='+this.state.tournamentId)
    .then(result => {
      let res = JSON.parse(result.text)
      if (res.status) {
        console.log('Fixtures', res)
        this.setState({ tournamentFixtures:res.data })
      }
    })

  }

  getTournamentData(){
    request
    .get(config.apiURL+'/tournament/v1/profile/?form_type=basic&tournament_id='+this.state.tournamentId)
    .then(result => {
      let res = JSON.parse(result.text)
      if (res.success) {
        console.log(res)
        this.setState({ tournamentData:res })
      }
    })
  }

  getTournamentSponsor(){
    request
    .get(config.apiURL+'/tournament/v1/tournamentSponsor/'+this.state.tournamentId)
    .then(result => {
      let res = JSON.parse(result.text)
      if (res.success) {
        console.log(res)
        this.setState({ tournamentSponsor:res.data })
      }
    })
  }

  getTeams(){
    request
    .get(config.apiURL+'/tournament/v1/teamRegisteration/'+ this.state.tournamentId +'/?form_type=approve')
    .then(result => {
      let res = JSON.parse(result.text)
      if (res.success) {
        console.log(res)
        this.setState({ tournamentTeams:res.data })
      }
    })
  }

  search(query){
    this.setState({ fetching:true}, ()=> {
      request
      // GET /api/venue/team_search/?query=de&tournament_id=119 HTTP/1.1
      .get(config.apiURL+'/venue/team_search/?query=' + query + '&tournament_id='+this.state.tournamentId)
      // .get(config.apiURL+'/search/v1/all_data/?query='+this.state.query)
      .then(result => {
        let res = JSON.parse(result.text)
        if (res.success) {
          console.log(res)
          this.setState({ allTeams:res.data, fetching:false })
        }
      })
    })
  }

  addTeam(){
    let teamId = this.refs.selectedTeam.rcSelect.state.value.pop()
    console.log(teamId)
    new Promise(function(resolve, reject) {
      resolve(teamId)
    })
    .then(teamId => {
      request
      .post(config.apiURL+'/tournament/v1/teamRegisteration/')
      .send({tournament_id:this.state.tournamentId,team_id, paid:"100", admin:99 })
      .then((result) => {
        let res = JSON.parse(result.text)
        if (res.success) {
          this.getTeams()
        }
      })
    })

  }

  addFixture(){
    new Promise(function(resolve, reject) {
      resolve(Math.random())
    })
    .then(rand => {
      let name = this.refs.gameweekName.value
      let begin_date = (this.refs.gameweekBeginDate.picker.state.value)
      let end_date = (this.refs.gameweekEndDate.picker.state.value)

      return {name, begin_date, end_date, type:'gameweek'}

    }).then((res) => {
      request
      // GET /api/venue/team_search/?query=de&tournament_id=119 HTTP/1.1
      .post(config.apiURL+'/tournament/v1/gameweek/')
      // .get(config.apiURL+'/search/v1/all_data/?query='+this.state.query)
      .send(res)
      .then(result => {
        let res = JSON.parse(result.text)
        if (res.status) {
          console.log(res)
          this.setState({ fixtureModal:false }, () => {
            this.getFixtures()
          })
        }
      })
    })
  }

  addSponsor(){
    console.log('Here')
    let tournamentId = this.state.tournamentId
    let name = this.refs.sponsorName.value
    let image = this.refs.sponsorImage.files[0]

    console.log('Sponsor', tournamentId, name, image)

    request
    .post(config.apiURL+'/tournament/v1/tournamentSponsor/')
    .field('tournament_id',tournamentId)
    .field('name', name)
    .attach('image', image)
    .then((result) => {
      let res = JSON.parse(result.text)
      if (res.success) {
        console.log(res)
        this.getTournamentSponsor()
        this.setState({ sponsorModal: false })
        // this.setState({ tournamentData:res })
      }

    })

  }

  removeSponsor(id){
    console.log(id)
    request
    .delete(config.apiURL+'/tournament/v1/tournamentSponsor/?id='+id)
    .then((result) => {
      let res = JSON.parse(result.text)
      if (res.success) {
        this.getTournamentSponsor()
      }
    })
  }

  uniqueKey(){
    return new Date().getTime() + Math.random()
  }

  render(){
    console.log('State', this.state)
    return (
    <div>
      {Object.keys(this.state.tournamentData).length > 0 &&
        <div className="row">
          <div className="col-md-6">
            <h6>Basic Information </h6>
          </div>
          <div className="col-md-6 d-none">
            <button className="btn btn-outline-dark col-md-6 offset-md-6">Edit Basic Information</button>
          </div>
        </div>
      }

      <div className="row mt-3">
        <div className="col-md-6">
          <div className="form-group row">
            <label htmlFor="inputName" className="col-sm-4 col-form-label"><h6>Name :</h6></label>
            <div className="col-sm-8">
              <input type="text" readOnly className="form-control-plaintext" id="inputName" value={this.state.tournamentData.name} />
            </div>
          </div>
          <div className="form-group row">
            <label htmlFor="startDate" className="col-sm-4 col-form-label"><h6>Start Date :</h6></label>
            <div className="col-sm-8">
              <input type="text" readOnly className="form-control-plaintext" id="startDate" value={this.state.tournamentData.start_date} />
            </div>
          </div>
          <div className="form-group row">
            <label htmlFor="endDate" className="col-sm-4 col-form-label"><h6>End Date :</h6></label>
            <div className="col-sm-8">
              <input type="text" readOnly className="form-control-plaintext" id="endDate" value={this.state.tournamentData.end_date} />
            </div>
          </div>
        </div>
        <div className="col-md-6">
          <div className="form-group row">
            <label htmlFor="noOfTeams" className="col-sm-4 col-form-label"><h6>No of Teams :</h6></label>
            <div className="col-sm-8">
              <input type="text" readOnly className="form-control-plaintext" id="noOfTeams" value={this.state.tournamentData.max_no_of_teams} />
            </div>
          </div>
          <div className="form-group row">
            <label htmlFor="prize" className="col-sm-4 col-form-label"><h6>Prize :</h6></label>
            <div className="col-sm-8">
              <input type="text" readOnly className="form-control-plaintext" id="prize" value={this.state.tournamentData.prize} />
            </div>
          </div>
        </div>
      </div>
      <hr/>
      <div className="row">
        <div className="col-md-6">
          <h6>Teams </h6>
        </div>
        <div className="col-md-6 d-none">
          <button className="btn btn-outline-dark col-md-6 offset-md-6" onClick = {() => {this.setState({ teamModal:true })}} >Add a Team</button>
        </div>
      </div>
      {/*
        'registered_teams' in this.state.tournamentData && this.state.tournamentData.registered_teams.length > 0 &&
         <Table columns = {teamsColumns} data = {this.state.tournamentData.registered_teams} />
      */}
      {this.state.tournamentTeams.length > 0 &&
        <Table key = {this.uniqueKey()} columns = {this.state.teamsColumns} data = {this.state.tournamentTeams} />
      }

      <hr/>
      <div className="row">
        <div className="col-md-6">
          <h6>Fixtures </h6>
        </div>
        <div className="col-md-6">
          <button className="btn btn-outline-dark col-md-6 offset-md-6" onClick = {() => {this.setState({ fixtureModal:true })}}>Add a Gameweek</button>
        </div>
      </div>
      {this.state.tournamentFixtures.length > 0 &&
        <Table key = {this.uniqueKey()} columns = {this.state.fixtureColumns} data = {this.state.tournamentFixtures} />
      }
      <hr/>
      <div className="row">
        <div className="col-md-6">
          <h6>Sponsor </h6>
        </div>
        <div className="col-md-6">
          <button className="btn btn-outline-dark col-md-6 offset-md-6" onClick = {() => {this.setState({ sponsorModal:true })}}>Add a Sponsor</button>
        </div>
      </div>
      {'sponsor' in this.state.tournamentData && this.state.tournamentData.sponsor.length > 0 &&
        <Table key = {this.uniqueKey()} columns = {this.state.sponsorColumns} data = {this.state.tournamentSponsor} />
      }
      {/* Modals are as follows */}
      <Modal
        title="Add a Team"
        wrapClassName="vertical-center-modal"
        visible={this.state.teamModal}
        onOk={this.addTeam.bind(this)}
        onCancel={() => this.setState({ teamModal:false })}
      >
        <form>

          <Select
            showSearch
            placeholder="Search Teams"
            notFoundContent={this.state.fetching ? <Spin size="small" /> : null}
            filterOption={false}
            ref="selectedTeam"
            onSearch={(query) => this.search(query)}
            onChange={(a,b,c) => console.log('change',a,b,c)}
            style={{ width: '100%' }}
          >
            {
              this.state.allTeams.map(d => <Option key={d.id}>{d.name}</Option>)
            }
          </Select>

        </form>
      </Modal>

      <Modal
        title="Add a Gameweek"
        wrapClassName="vertical-center-modal"
        visible={this.state.fixtureModal}
        onOk={this.addFixture.bind(this)}
        onCancel={() => this.setState({ fixtureModal:false })}
      >
        <form>
          <div className="form-group row">
            <label htmlFor="inputName" className="col-sm-4 col-form-label"><h6>Gameweek Name</h6></label>
            <div className="col-sm-8">
              <input type="text" required className="form-control" ref = "gameweekName" id="inputName" placeholder="Add Gameweek Name Here" />
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label"><h6>Begin Date</h6></label>
            <div className="col-sm-8">
              <DatePicker showTime format="YYYY-MM-DD HH:mm:ss" size={'large'} placeholder="Begin Date here " ref="gameweekBeginDate" onChange={(val) => console.log(val)} />
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label"><h6>End Date</h6></label>
            <div className="col-sm-8">
              <DatePicker showTime format="YYYY-MM-DD HH:mm:ss" size={'large'} placeholder="End Date here " ref="gameweekEndDate" onChange={(val) => console.log(val)} />
            </div>
          </div>

        </form>
      </Modal>

      <Modal
        title="Add a Sponsor"
        wrapClassName="vertical-center-modal"
        visible={this.state.sponsorModal}
        onOk={() => {console.log('Sponsor'), this.addSponsor()}}
        onCancel={() => this.setState({ sponsorModal:false })}
      >
        <form>
          <div className="form-group row">
            <label htmlFor="inputName" className="col-sm-4 col-form-label"><h6>Enter Sponsor</h6></label>
            <div className="col-sm-8">
              <input type="text" ref="sponsorName" className="form-control" id="inputName" placeholder="Add Sponsor Name Here" />
            </div>
          </div>
          <div className="form-group">
            <div className="input-group mb-3">
              <div className="input-group-prepend">
                <span className="input-group-text">Upload Sponsor Image</span>
              </div>
              <div className="custom-file">
                <input type="file" className="custom-file-input" id="logoUpload" ref="sponsorImage" />
                <label className="custom-file-label" htmlFor="logoUpload">Choose file</label>
              </div>
            </div>
          </div>
        </form>
      </Modal>

    </div>
    );
  }
}

export default Detail;
